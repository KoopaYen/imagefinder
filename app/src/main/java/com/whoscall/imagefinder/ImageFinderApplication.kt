package com.whoscall.imagefinder

import android.app.Application
import com.whoscall.imagefinder.component.DaggerRepositoryComponent
import com.whoscall.imagefinder.component.RepositoryComponent
import com.whoscall.imagefinder.model.PixabayRepository
import com.whoscall.imagefinder.module.RepositoryModule

class ImageFinderApplication : Application() {
    companion object {
        lateinit var repoComponent: RepositoryComponent
    }

    override fun onCreate() {
        super.onCreate()
        repoComponent = DaggerRepositoryComponent.builder()
                .repositoryModule(RepositoryModule(PixabayRepository()))
                .build()
    }
}
