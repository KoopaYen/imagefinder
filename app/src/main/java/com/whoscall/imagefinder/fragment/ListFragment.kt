package com.whoscall.imagefinder.fragment

import android.arch.lifecycle.LifecycleFragment
import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v7.widget.RecyclerView
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import com.alibaba.android.vlayout.DelegateAdapter
import com.alibaba.android.vlayout.LayoutHelper
import com.alibaba.android.vlayout.VirtualLayoutManager
import com.alibaba.android.vlayout.layout.LinearLayoutHelper
import com.bumptech.glide.Glide
import com.whoscall.imagefinder.R
import com.whoscall.imagefinder.activity.PhotoViewActivity
import com.whoscall.imagefinder.model.Hit
import com.whoscall.imagefinder.view_model.SharedViewModel
import org.jetbrains.anko.*
import org.jetbrains.anko.recyclerview.v7.recyclerView
import org.jetbrains.anko.support.v4.UI

/**
 * Created by jason.chang on 2017/6/7.
 */
class ListFragment : LifecycleFragment {

    lateinit var viewModel: SharedViewModel
    private var listAdapter: ListAdapter? = null

    constructor() : super()

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        viewModel = ViewModelProviders.of(activity).get(SharedViewModel::class.java)
        listAdapter!!.setViewModel(viewModel)
        viewModel.hit.observe(this, Observer<List<Hit>> {
            this.listAdapter?.notifyDataSetChanged()
        })
    }

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return UI {
            linearLayout {
                recyclerView {
                    id               = R.id.rvList
                    val manager      = VirtualLayoutManager(context)
                    layoutManager    = manager
                    val adapter      = DelegateAdapter(manager, true)
                    val helper       = LinearLayoutHelper()
                    helper.setDividerHeight(5)
                    listAdapter      = ListAdapter(helper)
                    adapter.addAdapter(listAdapter)
                    setAdapter(adapter)
                }.lparams {
                    width  = matchParent
                    height = matchParent
                }
            }
        }.view
    }

    class ListAdapter (val layoutHelper: LinearLayoutHelper) : DelegateAdapter.Adapter<ListViewHolder>() {

        private var sharedViewModel: SharedViewModel? = null

        fun setViewModel(viewModel: SharedViewModel) {
            this.sharedViewModel = viewModel
        }

        override fun onCreateLayoutHelper(): LayoutHelper {
            return layoutHelper
        }

        override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): ListViewHolder {
            return ListViewHolder(parent!!.context, ListItemUI().createView(AnkoContext.Companion.create(parent.context, parent)))
        }

        override fun onBindViewHolder(holder: ListViewHolder?, position: Int) {
            if (sharedViewModel != null) {
                holder!!.bind(sharedViewModel!!.hit.value!![position])
            }
        }

        override fun getItemCount(): Int {
            return if (sharedViewModel != null && sharedViewModel!!.hit.value != null) sharedViewModel!!.hit.value!!.size else 0
        }
    }

    class ListViewHolder(val context: Context, itemView: View) : RecyclerView.ViewHolder(itemView) {
        val img    = itemView.find<ImageView>(R.id.ivListItem)
        val llBase = itemView.find<LinearLayout>(R.id.llListItem)

        fun bind(hit: Hit) {
            llBase.onClick {
                val intent = Intent(context, PhotoViewActivity::class.java)
                intent.putExtra("pageUrl", hit.getWebformatURL())
                intent.putExtra("height", hit!!.getWebformatHeight())
                intent.putExtra("width", hit!!.getWebformatWidth())
                context.startActivity(intent)
            }

            Glide.with(context)
                    .load(hit.getWebformatURL())
                    .centerCrop()
                    .placeholder(R.mipmap.logo_square)
                    .into(img)
        }
    }

    class ListItemUI : AnkoComponent<ViewGroup> {
        override fun createView(ui: AnkoContext<ViewGroup>): View {
            return with(ui) {
                linearLayout {
                    lparams(width= matchParent, height = dip(200))
                    id          = R.id.llListItem
                    orientation = LinearLayout.VERTICAL

                    imageView {
                        lparams(width = matchParent, height = matchParent) {
                            gravity = Gravity.CENTER
                        }
                        id            = R.id.ivListItem
                        scaleType     = ImageView.ScaleType.FIT_CENTER
                        imageResource = R.mipmap.logo_square
                    }
                }
            }
        }
    }
}