package com.whoscall.imagefinder.fragment

import android.arch.lifecycle.LifecycleFragment
import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v7.widget.RecyclerView
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import com.alibaba.android.vlayout.DelegateAdapter
import com.alibaba.android.vlayout.LayoutHelper
import com.alibaba.android.vlayout.VirtualLayoutManager
import com.alibaba.android.vlayout.layout.StaggeredGridLayoutHelper
import com.bumptech.glide.Glide
import com.whoscall.imagefinder.R
import com.whoscall.imagefinder.activity.PhotoViewActivity
import com.whoscall.imagefinder.model.Hit
import com.whoscall.imagefinder.view_model.SharedViewModel
import org.jetbrains.anko.*
import org.jetbrains.anko.recyclerview.v7.recyclerView
import org.jetbrains.anko.support.v4.UI

/**
 * Created by jason.chang on 2017/6/7.
 */
class StaggeredGridFragment : LifecycleFragment {

    lateinit var viewModel: SharedViewModel
    private var staggeredAdapter: StaggeredAdapter? = null

    constructor() : super()

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        viewModel = ViewModelProviders.of(activity).get(SharedViewModel::class.java)
        staggeredAdapter!!.setViewModel(viewModel)
        viewModel.hit.observe(this, Observer<List<Hit>> {
            this.staggeredAdapter?.notifyDataSetChanged()
        })
    }

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return UI {
            linearLayout {
                recyclerView {
                    id               = R.id.rvList
                    val manager      = VirtualLayoutManager(context)
                    layoutManager    = manager
                    val adapter      = DelegateAdapter(manager, true)
                    staggeredAdapter = StaggeredAdapter(StaggeredGridLayoutHelper(3, 10))
                    adapter.addAdapter(staggeredAdapter)
                    setAdapter(adapter)
                }.lparams {
                    width  = matchParent
                    height = matchParent
                }
            }
        }.view
    }

    class StaggeredAdapter (val layoutHelper: StaggeredGridLayoutHelper) : DelegateAdapter.Adapter<StaggeredViewHolder>() {

        private var sharedViewModel: SharedViewModel? = null

        fun setViewModel(viewModel: SharedViewModel) {
            this.sharedViewModel = viewModel
        }

        override fun onCreateLayoutHelper(): LayoutHelper {
            return layoutHelper
        }

        override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): StaggeredViewHolder? {
            return StaggeredViewHolder(parent!!.context, StaggeredItemUI().createView(AnkoContext.create(parent.context, parent)))
        }

        override fun onBindViewHolder(holder: StaggeredViewHolder?, position: Int) {
            if (sharedViewModel != null) {
                holder!!.bind(sharedViewModel!!.hit.value!![position])
            }
        }

        override fun getItemCount(): Int {
            return if (sharedViewModel != null && sharedViewModel!!.hit.value != null) sharedViewModel!!.hit.value!!.size else 0
        }
    }

    class StaggeredViewHolder(val context: Context, itemView: View) : RecyclerView.ViewHolder(itemView) {
        val img    = itemView.find<ImageView>(R.id.ivStaggeredItem)
        val llBase = itemView.find<LinearLayout>(R.id.llStaggeredItem)

        fun bind(hit: Hit) {
            // Define the layout height by a random number
            val layoutParam = llBase.layoutParams
            layoutParam.height = 260 + hit.getPreviewHeight()!!.rem(7) * 20
            llBase.layoutParams = layoutParam

            llBase.onClick {
                val intent = Intent(context, PhotoViewActivity::class.java)
                intent.putExtra("pageUrl", hit.getWebformatURL())
                intent.putExtra("height", hit!!.getWebformatHeight())
                intent.putExtra("width", hit!!.getWebformatWidth())
                context.startActivity(intent)
            }

            Glide.with(context)
                    .load(hit.getPreviewURL())
                    .centerCrop()
                    .placeholder(R.mipmap.logo_square)
                    .into(img)
        }
    }

    class StaggeredItemUI : AnkoComponent<ViewGroup> {
        override fun createView(ui: AnkoContext<ViewGroup>): View {
            return with(ui) {
                linearLayout {
                    lparams(width= matchParent, height = dip(100))
                    id          = R.id.llStaggeredItem
                    orientation = LinearLayout.VERTICAL

                    imageView {
                        lparams(width = matchParent, height = matchParent) {
                            gravity = Gravity.CENTER
                        }
                        id            = R.id.ivStaggeredItem
                        scaleType     = ImageView.ScaleType.CENTER_CROP
                        imageResource = R.mipmap.logo_square
                    }
                }
            }
        }
    }
}