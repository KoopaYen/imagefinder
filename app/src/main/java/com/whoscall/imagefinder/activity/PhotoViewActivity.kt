package com.whoscall.imagefinder.activity

import android.app.Activity
import android.os.Bundle
import android.view.View
import com.bumptech.glide.Glide
import com.whoscall.imagefinder.R
import org.jetbrains.anko.*
import uk.co.senab.photoview.PhotoView

class PhotoViewActivity : Activity {

    constructor() : super()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        window.decorView.systemUiVisibility = View.SYSTEM_UI_FLAG_HIDE_NAVIGATION or View.SYSTEM_UI_FLAG_FULLSCREEN

        setContentView(R.layout.activity_photo_view)

        val progress = indeterminateProgressDialog(getString(R.string.loading))
        progress.show()

        async {
            val bitmap = Glide.with(applicationContext)
                    .load(intent.getStringExtra("pageUrl"))
                    .asBitmap()
                    .into(intent.getIntExtra("width", 100), intent.getIntExtra("height", 100))
                    .get()
            uiThread {
                progress.dismiss()
                find<PhotoView>(R.id.iv_photo).imageBitmap = bitmap
            }
        }
    }
}
