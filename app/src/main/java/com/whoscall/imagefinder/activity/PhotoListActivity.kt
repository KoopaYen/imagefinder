package com.whoscall.imagefinder.activity

import android.arch.lifecycle.LifecycleActivity
import android.arch.lifecycle.LifecycleFragment
import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import android.support.transition.AutoTransition
import android.support.transition.Fade
import android.support.transition.TransitionManager
import android.transition.Explode
import android.widget.Button
import android.widget.FrameLayout
import android.widget.LinearLayout
import com.whoscall.imagefinder.ImageFinderApplication
import com.whoscall.imagefinder.R
import com.whoscall.imagefinder.fragment.ListFragment
import com.whoscall.imagefinder.fragment.StaggeredGridFragment
import com.whoscall.imagefinder.model.Photo
import com.whoscall.imagefinder.view_model.PixabayViewModel
import com.whoscall.imagefinder.view_model.SharedViewModel
import org.jetbrains.anko.*
import javax.inject.Inject

class PhotoListActivity : LifecycleActivity {

    val FRAG_LIST = 0
    val FRAG_GRID = 1
    val fragHash = HashMap<Int, LifecycleFragment>()

    var isListFrag: Boolean

    @Inject lateinit var viewModel: PixabayViewModel

    constructor() : super() {
        fragHash.put(FRAG_LIST, ListFragment())
        fragHash.put(FRAG_GRID, StaggeredGridFragment())
        isListFrag = true
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        ImageFinderApplication.repoComponent.inject(this)

        linearLayout {
            orientation = LinearLayout.VERTICAL
            button {
                id           = R.id.btnSwitch
                textResource = R.string.grid
                onClick {
                    switchFragment()
                }
            }.lparams {
                width  = matchParent
                height = dip(48)
            }
            frameLayout {
                id = R.id.flPhotoList
                supportFragmentManager.beginTransaction().add(R.id.flPhotoList, fragHash[FRAG_LIST]).commit()
            }.lparams {
                width  = matchParent
                height = matchParent
            }
        }

        val progress = indeterminateProgressDialog(getString(R.string.loading))
        progress.show()

        val sharedViewModel = ViewModelProviders.of(this).get(SharedViewModel::class.java)
        viewModel.photo.observe(this, Observer<Photo> {
            it?.let {
                progress.dismiss()
                sharedViewModel.hit.value = it.getHits()
                if (viewModel.photo.value!!.getTotalHits() <= 0) {
                    alert(getString(R.string.no_photo)) { positiveButton(getString(R.string.ok)) { finish() } }.show()
                }
            }
        })
        viewModel.search(intent.getStringExtra("keyword"))
    }

    fun switchFragment() {
        // Change the button title dynamically
        find<Button>(R.id.btnSwitch).textResource = if (isListFrag) R.string.list else R.string.grid
        // Start the transition animation
        TransitionManager.beginDelayedTransition(find<FrameLayout>(R.id.flPhotoList), AutoTransition())
        // Change flag value
        isListFrag = !isListFrag
        supportFragmentManager.beginTransaction().replace(R.id.flPhotoList, fragHash[if (isListFrag) FRAG_LIST else FRAG_GRID]).commit()
    }
}
