package com.whoscall.imagefinder.activity

import android.content.Intent
import android.view.inputmethod.EditorInfo.IME_ACTION_DONE
import android.widget.Button
import android.widget.EditText
import com.whoscall.imagefinder.R
import org.jetbrains.anko.*

class MainActivity : android.arch.lifecycle.LifecycleActivity {

    lateinit var button: Button

    constructor() : super()

    override fun onCreate(savedInstanceState: android.os.Bundle?) {
        super.onCreate(savedInstanceState)

        relativeLayout {
            verticalLayout {
                editText {
                    id         = R.id.etSearch
                    hint       = getString(com.whoscall.imagefinder.R.string.searchBarHint)
                    singleLine = true
                    imeOptions = IME_ACTION_DONE
                    textChangedListener {
                        onTextChanged { text, start, before, count ->
                            button.enabled = !text!!.isEmpty()
                        }
                    }
                }.lparams {
                    height  = org.jetbrains.anko.wrapContent
                    width   = org.jetbrains.anko.wrapContent
                    gravity = android.view.Gravity.CENTER_HORIZONTAL
                }
                button(getString(com.whoscall.imagefinder.R.string.search)) {
                    id        = R.id.btnSearch
                    onClick { fetchResult() }
                    isEnabled = false
                }.lparams {
                    topMargin = 10
                    height    = org.jetbrains.anko.wrapContent
                    width     = org.jetbrains.anko.wrapContent
                    gravity   = android.view.Gravity.CENTER_HORIZONTAL
                }
            }.lparams {
                centerInParent()
                height = org.jetbrains.anko.wrapContent
                width  = org.jetbrains.anko.matchParent
            }
        }

        button = find<Button>(R.id.btnSearch)

    }

    fun fetchResult() {
        intent = Intent(this@MainActivity, PhotoListActivity::class.java)
        intent.putExtra("keyword", find<EditText>(R.id.etSearch).text.toString())
        startActivity(intent)
    }

}
