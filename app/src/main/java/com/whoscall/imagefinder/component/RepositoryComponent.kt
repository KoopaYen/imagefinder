package com.whoscall.imagefinder.component

import com.whoscall.imagefinder.activity.PhotoListActivity
import com.whoscall.imagefinder.activity.PhotoViewActivity
import com.whoscall.imagefinder.module.RepositoryModule
import dagger.Component
import javax.inject.Singleton

/**
 * Created by jason.chang on 2017/6/6.
 */
@Singleton
@Component(modules = arrayOf(RepositoryModule::class))
interface RepositoryComponent {
    fun inject(activity: PhotoListActivity)
    fun inject(activity: PhotoViewActivity)
}