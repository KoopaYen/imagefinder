package com.whoscall.imagefinder.module

import com.whoscall.imagefinder.model.PixabayRepository
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

/**
 * Created by jason.chang on 2017/6/6.
 */
@Module
class RepositoryModule {
    var repo: PixabayRepository

    constructor(repo: PixabayRepository) {
        this.repo = repo
    }

    @Provides
    @Singleton
    fun providePixabayRepository() : PixabayRepository {
        return this.repo
    }
}