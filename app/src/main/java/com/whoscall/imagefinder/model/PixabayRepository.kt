package com.whoscall.imagefinder.model

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import org.jetbrains.annotations.TestOnly
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton

/**
 * Created by jason.chang on 2017/6/5.
 */
@Singleton // informs Dagger that this class should be constructed once
class PixabayRepository {
    val API_KEY  = "5549878-b7e9354a00e20c93d4a8f6edf"
    val BASE_URL = "https://pixabay.com"

    private var pixabayService: PixabayService
    private val data = MutableLiveData<Photo>()

    init {
        val logger = HttpLoggingInterceptor()
        logger.level = HttpLoggingInterceptor.Level.BODY
        val httpClient = OkHttpClient.Builder().addInterceptor(logger).build()

        val retrofit = Retrofit.Builder()
                .baseUrl(BASE_URL)
                .client(httpClient)
                .addConverterFactory(GsonConverterFactory.create())
                .build()
        pixabayService = retrofit.create(PixabayService::class.java)
    }

    @TestOnly
    fun getServiceForUnitTest() : PixabayService {
        return pixabayService
    }

    fun getPhoto(keyword: String) : LiveData<Photo> {
        if (!keyword.isEmpty()) {
            pixabayService.getPhoto(API_KEY, keyword).enqueue(object : Callback<Photo> {
                override fun onResponse(call: Call<Photo>?, response: Response<Photo>?) {
                    data.value = response?.body()
                }

                override fun onFailure(call: Call<Photo>?, t: Throwable?) {}
            })
        } else {
            data.value = null
        }
        return data
    }
}