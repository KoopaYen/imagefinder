package com.whoscall.imagefinder.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

/**
 * Created by jason.chang on 2017/6/5.
 */
class Photo {
    @SerializedName("total")
    @Expose
    private var total: Int? = null

    @SerializedName("totalHits")
    @Expose
    private var totalHits: Int? = null

    @SerializedName("hits")
    @Expose
    private var hits: List<Hit>? = null

    fun setTotal(total: Int) {
        this.total = total
    }

    fun getTotal() : Int {
        return total ?: 0
    }

    fun setTotalHits(totalHits: Int) {
        this.totalHits = totalHits
    }

    fun getTotalHits() : Int {
        return totalHits ?: 0
    }

    fun setHits(hits: List<Hit>) {
        this.hits = hits
    }

    fun getHits(): List<Hit> {
        return hits ?: ArrayList<Hit>()
    }

}