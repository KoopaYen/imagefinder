package com.whoscall.imagefinder.model

import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query
import retrofit2.http.QueryMap

/**
 * Created by jason.chang on 2017/6/5.
 */
interface PixabayService {
    @GET("/api/")
    fun getPhoto(
        @Query("key") key: String,
        @Query("q", encoded = true) q: String) : Call<Photo>
}