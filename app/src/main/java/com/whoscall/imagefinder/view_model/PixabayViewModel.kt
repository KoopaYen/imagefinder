package com.whoscall.imagefinder.view_model

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.ViewModel
import com.whoscall.imagefinder.model.Photo
import com.whoscall.imagefinder.model.PixabayRepository
import javax.inject.Inject

/**
 * Created by jason.chang on 2017/6/5.
 */
class PixabayViewModel : ViewModel {
    var keyword: String
    var photo: LiveData<Photo>
    var repo: PixabayRepository

    @Inject // PixabayRepository parameter is provided by Dagger 2
    constructor(pixabayRepo: PixabayRepository) {
        keyword = ""
        repo    = pixabayRepo
        photo   = repo.getPhoto("")
    }

    fun search(keyword: String) {
        photo = repo.getPhoto(keyword)
    }

}