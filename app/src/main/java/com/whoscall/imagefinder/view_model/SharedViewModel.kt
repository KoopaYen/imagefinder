package com.whoscall.imagefinder.view_model

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel
import com.whoscall.imagefinder.model.Hit

/**
 * Created by jason.chang on 2017/6/7.
 */
class SharedViewModel : ViewModel {
    var hit = MutableLiveData<List<Hit>>()

    constructor() : super()

    fun getHits() : LiveData<List<Hit>> {
        return hit
    }
}