package com.whoscall.imagefinder;

import com.whoscall.imagefinder.model.Photo;
import com.whoscall.imagefinder.model.PixabayRepository;

import junit.framework.Assert;

import org.junit.Test;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by jason.chang on 2017/6/7.
 */

public class ApiTest {
    @Test
    public void api() throws Exception {
        PixabayRepository repo = new PixabayRepository();
        repo.getServiceForUnitTest().getPhoto("5549878-b7e9354a00e20c93d4a8f6edf", "dog").enqueue(new Callback<Photo>() {
            @Override
            public void onResponse(Call<Photo> call, Response<Photo> response) {
                Assert.assertNotNull(response.body());
            }

            @Override
            public void onFailure(Call<Photo> call, Throwable t) {
                Assert.assertTrue(call.isExecuted());
            }
        });
    }
}
